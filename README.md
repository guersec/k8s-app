# Creation of a Kubernetes Application
***
## Table of Contents
1. [About the project](#about-the-project)
2. [How to install it](#how-to-install-it)
3. [How it works](#how-it-works)
4. [Usefull information](#usefull-information)


## About the project
***
The purpose of our project is to create a kubernetes application.

In our case we decided to build a web application working with a msql database.  


## How to install it
***
Prerequisites:
> Before installing the project you need to have minikube started in your environment as well as kubeclt installed.

If you want to clone the project you can use the following command:
```
$ git clone https://gitlab.com/guersec/k8s-app.git
```

After that, in order to launch the application please enter the following command:
> If you don't want to clone the repo, you can run this command instead
```
$ kubectl apply -k https://gitlab.com/guersec/k8s-app.git
```

Now the application should be running. Follow the steps below to start using the app.


## How it works
***
To get the ip address of the application run the command below:
```
$ minikube service webapp --url 
```
This command should give you the connexion url, don't forget the port. 

After that, you should be able to start playing with our application.


## Start playing
***
Basically, our app allows you to create/list/delete user from a database

To create/delete a user fill "ajout user/suppression user" form

When created/deleted don't forget to click "return" to go back to "index.php" and see your changes

Enjoy :)


## Technical part
***

Our app is using two deployment to create 2 pods, 1 called WebApp using a custom docker image and another one mysql which is our database.

We configured also two services, a web service to be able to access our app form outside the cluster and a service for our database to access from inside the cluster.

Our mysql pod is configured when created, thanks to a configmap (database and tables are created).

Mysql is also configured to use a secret as well as a pvc to store the data.

And last but not least, the two pods are able to communicate.


## Usefull information
***
A list of links used within our project:
* https://gitlab.com/guersec/k8s-app
* https://hub.docker.com/r/guersec/monsite_image
